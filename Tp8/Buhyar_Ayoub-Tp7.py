import random
from random import shuffle
from ap_decorators import count
from timeit import timeit

def liste_alea(n:int):
    """construit une liste de longueur n contenant les entiers 0 à n-1 mélangés.

    Précondition : 
    Exemple(s) :
    $$$ 

    """
    res = list(range(0,n))
    shuffle(res)
    return res 
    

import matplotlib.pyplot as plt
