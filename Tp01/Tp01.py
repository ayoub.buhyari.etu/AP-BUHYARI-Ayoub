#Methode split
s = "la méthode split est parfois bien utile"
#>>> s.split(' ')
#['la', 'méthode', 'split', 'est', 'parfois','bien', 'utile']

#>>> s.split('e')
#['la méthod', ' split ', 'st parfois bi', 'n util', '']

#>>> s.split('é')
#['la m', 'thode split est parfois bien utile']

#>>> s.split()
#['la', 'méthode', 'split', 'est', 'parfois','bien', 'utile']

#pour s.split('') on a une erreur

#>>> s.split('split')
#['la méthode ', ' est parfois bien utile']

#2
#à chaque fois que dans une chaine de caractere la fonction rencontre le parametre elle crée une liste en decoupant et mettant la partie a droite et a gauche du parametre

#3
# oui elle modifie la chaine car on a plus une chaine mais une liste de chaine sans le parametre

#Methode join
l = s.split()

#2 la fonction join recolle la liste en rajoutant le parametre entre chaque element de la liste
#3 Ca crée une nouvelle chaine sans modifier lancienne chaine
#4
def join():
    """à_remplacer_par_ce_que_fait_la_fonction

    Précondition : 
    Exemple(s) :
    $$$ 

    """
    

#Méthode sort
#1 la liste est rangé par ordre alphabetique
#2 ca nous met une erreur car il ne peut pas mettre dans lordre 2 elements qui sont pas du meme type 


#Une fonction sort pour les chaines

def sort(s:str) -> str:
    """Renvoie une chaîne de caractères contenant les caractères de `s` triés dans l'ordre croissant.

    Précondition :  aucune
    Exemple(s) :
    $$$ sort('timoleon')
    'eilmnoot'
    """
    l = list(s)
    l.sort()
    return "".join(l)

#Anagrammes
#Version 1
def sont_anagrammes(s1: str, s2: str) -> bool:
    """
    Renvoie True si s1 et s2 sont anagrammatiques, False sinon.

    Précondition: aucune

    Exemples:

    $$$ sont_anagrammes('orange', 'organe')
    True
    $$$ sont_anagrammes('orange','Organe')
    True
    """
    s1 = list(s1)
    s2 = list(s2)
    s1.sort()
    s2.sort()
    for i in range(len(s1)):
        if s1[i] == s2[i]:
            res = True
        else:
            res = False
    return res and len(s1) == len(s2)
#version2
def sont_anagrammes2(s1: str, s2: str) -> bool:
    """
    Renvoie True si s1 et s2 sont anagrammatiques, False sinon.

    Précondition: aucune

    Exemples:

    $$$ sont_anagrammes('orange', 'organe')
    {'o': 1, 'r': 1, 'a': 1, 'n': 1, 'g': 1, 'e': 1}
    """
    res1 = {}
    res2 = {}
    for l1 in s1:
        res1[l1] = res1.get(l1, 0) + 1
    for l2 in s2:
        res2[l2] = res2.get(l2, 0) + 1
    return res1 == res2

#Version3
def sont_anagrammes3(s1: str, s2: str) -> bool:
    """
    Renvoie True si s1 et s2 sont anagrammatiques, False sinon.

    Précondition: aucune

    Exemples:
    """
    res = True
    for c in s1:
        if s1.count(c) != s2.count(c):
            res= False
    return res and len(s1) == len(s2)

#Casse et accentuation
#1
EQUIV_NON_ACCENTUE = {'ç':'c', 'à':'a', 'ñ':'n', 'è':'e', 'é':'e', 'ù':'u', 'I̧':'i', 'Í':'i', 'ê':'e'}
#2
def bas_casse_sans_accent(chaine:str) -> str:
    """envoie une chaîne de caractères identiques à celle passée en paramètre sauf pour les lettres majuscules et les lettres accentuées qui sont converties en leur équivalent minuscule non accentué.
    Précondition : parametre chaine de caractere 
    Exemple(s) :
    $$$ bas_casse_sans_accent('Orangé')
    'orange'
    """
    nv_chaine = ""
    chaine2 = chaine.lower()
    for c in chaine2:
        if c in EQUIV_NON_ACCENTUE :
            nv_chaine = nv_chaine + EQUIV_NON_ACCENTUE[c]
        else:
            nv_chaine = nv_chaine + c
    return nv_chaine

def sont_anagrammes(s1:str,s2:str) -> bool:
    """

    Précondition : 
    Exemple(s) :
    $$$ 

    """
    s1 = bas_casse_sans_accent(s1)
    s2 = bas_casse_sans_accent(s2)
    return sont_anagrammes3(s1,s2)
    
    
#Recherche d'anagrammes