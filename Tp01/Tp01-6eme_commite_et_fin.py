#Methode split
s = "la méthode split est parfois bien utile"
#>>> s.split(' ')
#['la', 'méthode', 'split', 'est', 'parfois','bien', 'utile']

#>>> s.split('e')
#['la méthod', ' split ', 'st parfois bi', 'n util', '']

#>>> s.split('é')
#['la m', 'thode split est parfois bien utile']

#>>> s.split()
#['la', 'méthode', 'split', 'est', 'parfois','bien', 'utile']

#pour s.split('') on a une erreur

#>>> s.split('split')
#['la méthode ', ' est parfois bien utile']

#2
#à chaque fois que dans une chaine de caractere la fonction rencontre le parametre elle crée une liste en decoupant et mettant la partie a droite et a gauche du parametre

#3
# oui elle modifie la chaine car on a plus une chaine mais une liste de chaine sans le parametre

#Methode join
l = s.split()

#2 la fonction join recolle la liste en rajoutant le parametre entre chaque element de la liste
#3 Ca crée une nouvelle chaine sans modifier lancienne chaine
#4
def join(s:str,l:list) -> str:
    """
renvoie une chaîne de caractères construite en concaténant toutes les chaînes de l en intercalant s sans utiliser la méthode homonyme

    Précondition : 
    Exemple(s) :
    $$$ join('.', ['raymond', 'calbuth', 'ronchin', 'fr'])
    'raymond.calbuth.ronchin.fr'
    """
    chaine = ""
    for element in l:
        chaine += element + s
    return chaine

#Méthode sort
#1 la liste est rangé par ordre alphabetique
#2 ca nous met une erreur car il ne peut pas mettre dans lordre 2 elements qui sont pas du meme type 


#Une fonction sort pour les chaines

def sort(s:str) -> str:
    """Renvoie une chaîne de caractères contenant les caractères de `s` triés dans l'ordre croissant.

    Précondition :  aucune
    Exemple(s) :
    $$$ sort('timoleon')
    'eilmnoot'
    """
    l = list(s)
    l.sort()
    return "".join(l)

#Anagrammes
#Version 1
def sont_anagrammes(s1: str, s2: str) -> bool:
    """
    Renvoie True si s1 et s2 sont anagrammatiques, False sinon.

    Précondition: aucune

    Exemples:

    $$$ sont_anagrammes('orange', 'organe')
    True
    $$$ sont_anagrammes('orange','Organe')
    True
    """
    s1 = list(s1)
    s2 = list(s2)
    s1.sort()
    s2.sort()
    for i in range(len(s1)):
        if s1[i] == s2[i]:
            res = True
        else:
            res = False
    return res and len(s1) == len(s2)
#version2
def sont_anagrammes2(s1: str, s2: str) -> bool:
    """
    Renvoie True si s1 et s2 sont anagrammatiques, False sinon.

    Précondition: aucune

    Exemples:

    $$$ sont_anagrammes('orange', 'organe')
    {'o': 1, 'r': 1, 'a': 1, 'n': 1, 'g': 1, 'e': 1}
    """
    res1 = {}
    res2 = {}
    for l1 in s1:
        res1[l1] = res1.get(l1, 0) + 1
    for l2 in s2:
        res2[l2] = res2.get(l2, 0) + 1
    return res1 == res2
    

#Version3
def sont_anagrammes3(s1: str, s2: str) -> bool:
    """
    Renvoie True si s1 et s2 sont anagrammatiques, False sinon.

    Précondition: aucune

    Exemples:
    """
    res = True
    for c in s1:
        if s1.count(c) != s2.count(c):
            res= False
    return res and len(s1) == len(s2)

#Casse et accentuation
#1
EQUIV_NON_ACCENTUE = {
    'à': 'a',
    'â': 'a',
    'ä': 'a',
    'ç': 'c',
    'é': 'e',
    'è': 'e',
    'ê': 'e',
    'ë': 'e',
    'î': 'i',
    'ï': 'i',
    'ô': 'o',
    'ö': 'o',
    'ù': 'u',
    'û': 'u',
    'ü': 'u',
    'ÿ': 'y',
    'À': 'A',
    'Â': 'A',
    'Ä': 'A',
    'Ç': 'C',
    'É': 'E',
    'È': 'E',
    'Ê': 'E',
    'Ë': 'E',
    'Î': 'I',
    'Ï': 'I',
    'Ô': 'O',
    'Ö': 'O',
    'Ù': 'U',
    'Û': 'U',
    'Ü': 'U',
    'Ÿ': 'Y'
}

#2
def bas_casse_sans_accent(chaine:str) -> str:
    """envoie une chaîne de caractères identiques à celle passée en paramètre sauf pour les lettres majuscules et les lettres accentuées qui sont converties en leur équivalent minuscule non accentué.
    Précondition : parametre chaine de caractere 
    Exemple(s) :
    $$$ bas_casse_sans_accent('Orangé')
    'orange'
    """
    nv_chaine = ""
    chaine2 = chaine.lower()
    for c in chaine2:
        if c in EQUIV_NON_ACCENTUE :
            nv_chaine = nv_chaine + EQUIV_NON_ACCENTUE[c]
        else:
            nv_chaine = nv_chaine + c
    return nv_chaine

def sont_anagrammes4(s1:str,s2:str) -> bool:
    """
Renvoie True si S1 et S2 sont anagrammes False sinon
    Précondition : aucune
    Exemple(s) :
    $$$ sont_anagrammes('Orangé', 'organE')
    True
    """
    return sont_anagrammes3(bas_casse_sans_accent(s1),bas_casse_sans_accent(s2))
    
    
#Recherche d'anagrammes
from lexique import LEXIQUE
#1 ce lexique contient 139719 mots
#2 Il n'a aucun doublon


# Anagrammes d'un mot : première méthode
#1
def anagrammes(mot: str) -> list[str]:
    """
    Renvoie la liste des mots figurant dans le LEXIQUE qui sont des anagrammes de mot.

    Précondition: aucune

    Exemples:

    $$$ anagrammes('orange')
    ['onagre', 'orange', 'orangé', 'organe', 'rongea']
    $$$ anagrammes('info')
    ['foin']
    $$$ anagrammes('Calbuth')
    []
    """
    return [m for m in LEXIQUE if sont_anagrammes4(m,mot)]
    
    

#2 chien possede 2 annagrames  lui meme et niche 
#anagrammes("chien")
#['chien', 'niche']

#Anagrammes d'un mot : seconde méthode

#1 car il y'a beaucoup de mots ca serait trop couteux et une clé est unique
#2
def cle(mot: str) -> str:
    """
   calcule la clé que doit avoir un mot donné dans le dictionnaire.

    Précondition: aucune

    Exemples:

    $$$ cle('Orangé')
    'aegnor'
    """
    res = bas_casse_sans_accent(mot)
    return sort(res)




ANAGRAMMES = {}
for mot in LEXIQUE:
    clé = cle(mot)
    if clé not in ANAGRAMMES:
        ANAGRAMMES[clé] = [mot]
    else:
        ANAGRAMMES[clé].append(mot)

def anagramme_avec_dico(chaine:str) -> list[str]:
     """
    Renvoie la liste des mots figurant dans le LEXIQUE qui sont des anagrammes de mot.

    Précondition: aucune

    Exemples:

    $$$ anagramme_avec_dico('orange')
    ['onagre', 'orange', 'orangé', 'organe', 'rongea']
    $$$ anagramme_avec_dico('info')
    ['foin']
    $$$ anagramme_avec_dico('Calbuth')
    []
    """
     if cle(chaine) in ANAGRAMMES:
         res = ANAGRAMMES[cle(chaine)]
     else:
        res = []
     return res 
         
#2 >>> anagramme_avec_dico('chien')
#['chien', 'chiné', 'niche', 'niché']
    
    
# Phrases d'anagrammes
from copy import deepcopy

def permutation(chaine:str) -> list[str]:
    """
"""
    res = []
    liste = chaine.split()
    for mot in liste:
        anagrammes = anagramme_avec_dico(mot)
        for j in anagrammes:
            i = liste.index(mot)
            t = deepcopy(liste)
            t[i] = j
            res.append(" ".join(t))
    return res

def anagrammes_phrase(chaine:str) -> list[str]:
    """
    prend une phrase et renvoie la liste des phrases obtenues en remplaçant chacun des mots par leurs anagrammes
    precondition: le parametre doit etre une chaine de mot

    Exemples:

    """
    res = []
    liste1 = permutation(chaine)
    for phrase in liste1:
        res += permutation(phrase)
        liste = sorted(list(set(res)))
    return liste

#>>> anagrammes_phrase('Mange ton orange')
#['Mange ont onagre', 'Mange ont orange', 'Mange ont orangé', 'Mange ont organe', 'Mange ont rongea', 'Mange ton onagre', 'Mange ton orange', 'Mange ton orangé', 'Mange ton organe', 'Mange ton rongea', 'mange ont orange', 'mange ton onagre', 'mange ton orange', 'mange ton orangé', 'mange ton organe', 'mange ton rongea', 'mangé ont orange', 'mangé ton onagre', 'mangé ton orange', 'mangé ton orangé', 'mangé ton organe', 'mangé ton rongea']
