#BUHYARI Ayoub
from Bloc import Bloc
from PIL import Image, ImageDraw
import csv
from algo_rec import * 
def cvs_to_blocks(fichier_csv:str) -> list[Bloc]:
    """convertie un fichier csv en un block

    Précondition : fichier_csv le chemin d'un fichier du type .csv
    Exemple(s) :
    $$$ 

    """
    liste_bloc = []
    with open(fichier_csv, 'r') as fichier:
        for ligne in fichier:
            ligne = ligne.strip().split(',')
            if len(ligne) == 7:
                valeur_ligne = [int(valeur) for valeur in ligne]
                px_hg = (valeur_ligne[0],valeur_ligne[1])
                px_bd = (valeur_ligne[2],valeur_ligne[3])
                couleur = (valeur_ligne[4],valeur_ligne[5],valeur_ligne[6])
                bloc = Bloc(px_hg,px_bd,couleur)
                liste_bloc.append(bloc)
    return liste_bloc

def bloc_to_image(nouvelle_image: str, liste_bloc) -> Image:
    """
    Crée une image et convertit une liste de bloc en une image.

    Précondition : 
    """
    width, height = 256, 256
    image = Image.new("RGB", (width, height), "black")
    draw = ImageDraw.Draw(image)
    if isinstance(liste_bloc, Bloc):  
        draw.rectangle([liste_bloc.px_hg, liste_bloc.px_bd], fill=liste_bloc.couleur)
    else:
        for bloc in liste_bloc:
            draw.rectangle([bloc.px_hg, bloc.px_bd], fill=bloc.couleur)
    image.show()
    return image.save("./image_recursif/" + nouvelle_image + ".png", "PNG")



#PASSER D'UN FICHIER CSV A UNE IMAGE
def CSV_to_Image(fichier_csv, nouvelle_image) ->Image:
    """
    Convertit un fichier CSV en une image.

    precondition: fichier_csv (str): Le chemin vers le fichier CSV.
                  nouvelle_image: chemin de la nouvelle image du style "nouvelle_image.png"
    """
    liste_bloc = cvs_to_blocks(fichier_csv)
    

    max_x = max(bloc.px_bd[0] for bloc in liste_bloc)
    max_y = max(bloc.px_bd[1] for bloc in liste_bloc)
    
    width = max_x + 1
    height = max_y + 1
    
    # Créer une nouvelle image
    image = Image.new("RGB", (width, height), "black")
    
    # Dessiner les blocs sur l'image
    draw = ImageDraw.Draw(image)
    for bloc in liste_bloc:
        draw.rectangle([bloc.px_hg, bloc.px_bd], fill=bloc.couleur)
    
    image.show()
    
    return image.save(nouvelle_image, "PNG")
#######################################################################################################################
    
def bloc_to_csv(liste_bloc: list[Bloc], fichier_csv: str) -> None:
    """
    Convertit une liste de blocs dans un document CSV.
    """
    with open("./csv/" + fichier_csv, 'w') as fichier:
        writer = csv.writer(fichier)
        for bloc in liste_bloc:
            res = []
            couleur = bloc.couleur
            px_hg = bloc.px_hg
            px_bd = bloc.px_bd
            res.append(px_hg[0])
            res.append(px_hg[1])
            res.append(px_bd[0])
            res.append(px_bd[1])
            res.append(couleur[0])
            res.append(couleur[1])
            res.append(couleur[2])
            writer.writerow(res)

#PASSER D'UNE IMAGE A UN DOCUMENT CSV
def image_to_csv(liste_bloc:list[Bloc] , fichier_csv:str)-> None:
    """
enregistre l'image dans un document CSV que l'on souhaite cree
precondition: fichier csv du styler "fichier_csv.csv"
"""
    bloc_to_csv(liste_bloc,fichier_csv)
    
def image_to_csv_bloc(bloc:Bloc,fichier_csv:str)->None:
    """
convertie une image en csv
"""
    with open("./csv/" + fichier_csv, 'w') as fichier:
        writer = csv.writer(fichier)
        res = []
        couleur = bloc.couleur
        px_hg = bloc.px_hg
        px_bd = bloc.px_bd
        res.append(px_hg[0])
        res.append(px_hg[1])
        res.append(px_bd[0])
        res.append(px_bd[1])
        res.append(couleur[0])
        res.append(couleur[1])
        res.append(couleur[2])
        writer.writerow(res)

