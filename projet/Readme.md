- - -
Title: Projet  
author: Buhyari Ayoub
- - -
#journal
POUR LES ORDRES >4 IL FAUT ATTENDRE UN PEU DE TEMPS QUE LES CALCULES SE FONT SUR LE TERMINAL POUR QUE CELA S'AFFICHE (exemple 30 secondes pour l'ordre 5)

Mercredi 27/03: realisation de la class Bloc
Jeudi 28/03: comprehension de la recurcivité + fonction decoupage : couleur_proche et fusion_4_bloc que je sais pas comment relier a mon algo principale image_rec
Samedi 29/03:
realisation d'une fonction qui convertie image en bloc et fonction qui divise bloc recurcivement en liste de sous_bloc (une liste avec des blocs) -> pas liste de liste de bloc etc
Lundi 01/04: partie ligne de commande fini avec 3 commandes - une pour  enregistrer l'image  au format png
          - une pour enregistrer l'image au format csv
          - une pour afficher l'image seulement qui est elle meme au format png au debut et a la fin de l'algo
          -affiche un document csv en une image 
            



#Documentation
Fichier n*1: Decoupage
(1)
couleur_proche : parametre:liste_bloc return: bool
description:
calcule si une liste de 4 bloc sont de couleur proche

(2)
fusion_4_bloc: parametre:liste_bloc return: Bloc
description:
fusionne 4 blocs pour en former 1 (utile pour couleur_proche)

(3)
couleur_moyenne_image: parametre:Image return: tuple (r,v,b)
description:
calcule la moyenne d'une image donnée (utile pour ordre 0)

(4)
image_to_block: parametre: image return: Bloc
convertie une image en un bloc sans couleur

(5)
divise_bloc: parametre:bloc ordre:entier>0
description:
divise un bloc en plusieur sous bloc selon l'ordre choisit 

(6)
affiche_liste_bloc: parametre:liste de bloc
description:
convertie une liste de bloc en une image

(7)
couleur_moyenne_bloc: parametre:image et un bloc return:couleur moyenne dune bloc
description:
prend un bloc dune image et fait la moyenne des couleurs de ce bloc et renvoie une couleur 

Fichier 2: algo_rec
(1)
image_rec: parametre:image , ordre>0 return:liste de bloc
description:
renvoie une liste de bloc donc l'image decoupait selon un ordre

fichier 3: enregistrement_lecture_image
(1)
csv_to_blocks: parametre: un fichier csv return: liste de bloc
description:
convertie un fichier csv en une liste de bloc 


(2)
bloc_to_image: parametre: nouvelle_image , liste de bloc return: une image
description:
convertie un bloc en une image et l'enregistre

(3)
CSV_to_Image: parametre:fichier_csv, nouvelle_image return:image
description:
convertie un fichier csv en une image

(4)
bloc_to_csv: parametre:liste_bloc et un fichier_csv return: un fichier csv

(5)
image_to_csv: parametre: liste de bloc et un fichier_csv return None
description:
convertie une image en un document csv

Fichier 4: image_rec
fichier qui relie le programme a mon projet

(1) commande 1  : python image_rec.py ./Images/fichier.png ordre nouvelle_image enregistre-png

parametre: image , ordre , nouvelle_image enregistre-png
description:
enregistre une image png apres lui avoir appliquer l'algo recurcive

Exemple: python image_rec.py ./Images/calbuth.png 3 calbuth_ordre_3 enregistre-png

(2) commande 2 :python image_rec.py image ordre nouveau_fichier_csv  enregistre-csv

parametre: image , ordre , nouvelle_image enregistre-csv
Description:
enregistre une image png dans un document csv 

Exemple: python image_rec.py ./Images/calbuth.png 3 csv_calbuth_3 enregistre-csv

(3) commande 3 : python image_rec.py chemin_vers_image ordre affiche-png 

parametre  : chemin vers image , ordre affiche-png
Description
affiche un document png decoupait selon lordre apres avoir appliquer l'algo_rec

Exemple: python image_rec.py ./Images/joconde.png 3 affiche-png

affiche la joconde a l'ordre 3


(4) commande 4 : python image_rec.py document_csv affiche-csv
parametre : chemin vers fichier csv 
Description:
affiche un document csv 

Exemple : python image_rec.py ./Images/mystere.csv affiche-csv

affiche le document mystere
