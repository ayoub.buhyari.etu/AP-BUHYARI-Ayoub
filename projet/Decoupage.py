#BUHYARI Ayoub
#Partie decoupage
from Bloc import Bloc
from PIL import Image , ImageDraw
from algo_rec import *
from PIL import *

def couleur_proche(liste_bloc:list[Bloc] , seuil=20):
    """
si les 4 blocs sont de couleur proche renvoie True , Flase sinon
precondition: 4 elements blocs dans liste_bloc avec chacun une couleur
""" 
    for i in range(3):
        couleur1 = liste_bloc[i].couleur
        couleur2 = liste_bloc[i+1].couleur
        max_diff = max(abs(couleur1[j] - couleur2[j]) for j in range(3))
        if max_diff > seuil:
            return False
    return True

        
def fusion_4_bloc(liste_bloc:list[Bloc]):
    """
fusionne 4 bloc pour en former un seul
precondition: len(liste_bloc) == 4
"""
    somme_r = 0
    somme_v = 0
    somme_b = 0
    px_bd_x = max(liste_bloc[0].px_bd[0],liste_bloc[1].px_bd[0],liste_bloc[2].px_bd[0],liste_bloc[3].px_bd[0])
    px_bd_y = max(liste_bloc[0].px_bd[1],liste_bloc[1].px_bd[1],liste_bloc[2].px_bd[1],liste_bloc[3].px_bd[1])
    px_hg_x= min(liste_bloc[0].px_hg[0],liste_bloc[1].px_hg[0],liste_bloc[2].px_hg[0],liste_bloc[3].px_hg[0])
    px_hg_y = min(liste_bloc[0].px_hg[1],liste_bloc[1].px_hg[1],liste_bloc[2].px_hg[1],liste_bloc[3].px_hg[1])
    for bloc in liste_bloc:
        r,v,b = bloc.couleur
        somme_r = somme_r + r
        somme_v = somme_v + v
        somme_b = somme_b + b
    return Bloc((px_hg_x,px_hg_y),(px_bd_x,px_bd_y),(somme_r/4,somme_b/4,somme_b/4))

def couleur_moyenne_image(image:Image):
    """
Calcule la couleur moyenne dune image pour l'ordre 0
precondition: aucune
"""
    im_rgb = Image.open(image).convert("RGB") 
    largeur, hauteur = im_rgb.size
    total_r = total_g = total_b = 0

    for y in range(hauteur):
        for x in range(largeur):
            couleur_pixel = im_rgb.getpixel((x, y))
            total_r += couleur_pixel[0]
            total_g += couleur_pixel[1]
            total_b += couleur_pixel[2]

    nb_pixels = largeur * hauteur
    couleur_moy_r = total_r // nb_pixels
    couleur_moy_g = total_g // nb_pixels
    couleur_moy_b = total_b // nb_pixels

    return (couleur_moy_r, couleur_moy_g, couleur_moy_b)

def image_to_block(image_path):
    image = Image.open(image_path).convert("RGB")
    largeur, hauteur = image.size
    return Bloc((0, 0), (largeur, hauteur), (None, None, None))

def divise_bloc(image_path: str, bloc: Bloc, ordre: int):
    """
    Divise l'image en 4 sous-blocs et le fait récursivement jusqu'à ce que l'ordre soit atteint.
    """
    sous_blocs = []

    if ordre == 1:
        px_hg, px_bd = bloc.px_hg, bloc.px_bd
        largeur = px_bd[0] - px_hg[0]
        hauteur = px_bd[1] - px_hg[1]
        demi_largeur = largeur // 2
        demi_hauteur = hauteur // 2

        sous_blocs.append(Bloc(px_hg, (px_hg[0] + demi_largeur, px_hg[1] + demi_hauteur), (None, None, None)))  
        sous_blocs.append(Bloc((px_hg[0] + demi_largeur, px_hg[1]), (px_bd[0], px_hg[1] + demi_hauteur), (None, None, None)))  
        sous_blocs.append(Bloc((px_hg[0], px_hg[1] + demi_hauteur), (px_hg[0] + demi_largeur, px_bd[1]), (None, None, None)))  
        sous_blocs.append(Bloc((px_hg[0] + demi_largeur, px_hg[1] + demi_hauteur), px_bd, (None, None, None)))  

    else:
        demi_largeur = (bloc.px_bd[0] - bloc.px_hg[0]) // 2
        demi_hauteur = (bloc.px_bd[1] - bloc.px_hg[1]) // 2

        sous_blocs += divise_bloc(image_path, Bloc(bloc.px_hg, (bloc.px_hg[0] + demi_largeur, bloc.px_hg[1] + demi_hauteur), (None, None, None)), ordre - 1)
        sous_blocs += divise_bloc(image_path, Bloc((bloc.px_hg[0] + demi_largeur, bloc.px_hg[1]), (bloc.px_bd[0], bloc.px_hg[1] + demi_hauteur), (None, None, None)), ordre - 1)
        sous_blocs += divise_bloc(image_path, Bloc((bloc.px_hg[0], bloc.px_hg[1] + demi_hauteur), (bloc.px_hg[0] + demi_largeur, bloc.px_bd[1]), (None, None, None)), ordre - 1)
        sous_blocs += divise_bloc(image_path, Bloc((bloc.px_hg[0] + demi_largeur, bloc.px_hg[1] + demi_hauteur), bloc.px_bd, (None, None, None)), ordre - 1)
        
    for bloc in sous_blocs:
        bloc.couleur = couleur_moyenne_bloc(image_path, bloc)
    
    return sous_blocs
    
def afficher_liste_blocs(liste_blocs:list[Bloc]):
    """
    convertie une liste de bloc en une image
    precondition: liste_blocs non vide
    """

    max_x = max(bloc.px_bd[0] for bloc in liste_blocs)
    max_y = max(bloc.px_bd[1] for bloc in liste_blocs)


    image = Image.new("RGB", (max_x + 1, max_y + 1), "white")

  
    draw = ImageDraw.Draw(image)
    for bloc in liste_blocs:
        draw.rectangle([bloc.px_hg, bloc.px_bd], fill=bloc.couleur)

    image.show()
    

def couleur_moyenne_bloc(image:Image, bloc:Bloc):
    """
calcule la couleur moyenne dune bloc
precondition: 
"""
    im = Image.open(image)
    im_rgb = im.convert("RGB")

    total_r = 0
    total_g = 0
    total_b = 0
    for y in range(bloc.px_hg[1], bloc.px_bd[1]):
        for x in range(bloc.px_hg[0], bloc.px_bd[0]):
            couleur_pixel = im_rgb.getpixel((x, y))
            total_r += couleur_pixel[0]
            total_g += couleur_pixel[1]
            total_b += couleur_pixel[2]

    nb_pixels = (bloc.px_bd[0] - bloc.px_hg[0]) * (bloc.px_bd[1] - bloc.px_hg[1])
    couleur_r = total_r // nb_pixels
    couleur_g = total_g // nb_pixels
    couleur_b = total_b // nb_pixels

    return (couleur_r, couleur_g, couleur_b)
