#BUHYARI Ayoub
#ligne de commande
from algo_rec import *
from enregistrement_lecture_image import *
from Decoupage import *
from PIL import * 
if __name__ == "__main__":
    """
applique algorithme recurcive a une image selon un ordre et l'enregistre dans une nouvelle image
"""
    import sys
    if len(sys.argv) != 5:
        print("usage: image_rec.py image ordre nouvelle_image enregistre-png")
    else:
        image, ordre, nouvelle_image, mode = sys.argv[1:]
        if mode == "enregistre-png":
            image = str(image)
            ordre = int(ordre)
            nouvelle_image = str(nouvelle_image)
            bloc = image_rec(image,ordre)
            bloc_to_image(nouvelle_image, bloc)
            
if __name__ == "__main__":
    """"
    Applique un algorithme récursif à une image selon un ordre et l'enregistre dans un document CSV.
    """
    import sys
    if len(sys.argv) != 5:
        print("usage: image_rec.py image ordre nouveau_fichier_csv enregistre-csv")
    else:
        image, ordre, nouveau_fichier_csv, mode = sys.argv[1:]
        if mode == "enregistre-csv":
            image = str(image)
            ordre = int(ordre)
            nouveau_fichier_csv = str(nouveau_fichier_csv)
            if ordre >0:
                nouveau_fichier_csv = str(nouveau_fichier_csv)
                bloc = image_rec(image, ordre)
                image_to_csv(bloc, nouveau_fichier_csv + ".csv")
            elif ordre==0:
                bloc = image_rec(image, ordre)
                image_to_csv_bloc(bloc,nouveau_fichier_csv)
             
            
if __name__ == "__main__":
    """
applique algorithme recurcive a une image selon un ordre l'affiche

"""
    import sys
    if len(sys.argv) != 4:
        print("usage: image_rec.py image ordre affiche-png")
    else:
        image, ordre, mode = sys.argv[1:]
        if mode == "affiche-png":
            image = str(image)
            ordre = int(ordre)
            res = image_rec(image, ordre)
            afficher_liste_blocs(res)
        else:
            print("usage: image_rec.py image ordre affiche-png")
            

if __name__ == "__main__":
    """
affiche un fichier csv
"""
    import sys
    if len(sys.argv) != 3:
        print("usage: image_rec.py fichier_csv affiche-csv")
    else: 
        fichier_csv,mode = sys.argv[1:]
        if mode == "affiche-csv":
            fichier_csv = str(fichier_csv)
            res = cvs_to_blocks(fichier_csv)
            afficher_liste_blocs(res)
