#Buhyari Ayoub
#classe bloc pour manipuler et structurer l'image
class Bloc:
    """Paramètres :
   Initialise un bloc qui represente une partie de l'image
   argument:
        - px_hg = pixel haut gauche
        - px_bd = pixel bas droit
        - couleur = tuple de 3 valeur : r,v et b 
    """
    def __init__(self,px_hg,px_bd,couleur):
        """ Initialise un bloc avec ses coins supérieur gauche et inférieur droit et sa couleur.
        """
        self.px_hg = px_hg
        self.px_bd = px_bd
        self.couleur = couleur
        self.size = (int(px_bd[0] - px_hg[0]), int(px_bd[1] - px_hg[1]))
    
    def __str__(self):
        return f"Bloc({self.px_hg},{self.px_bd},{self.couleur})"
    
    def __repr__(self):
        return f"Bloc({self.px_hg},{self.px_bd},{self.couleur})"
