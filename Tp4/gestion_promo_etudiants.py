
def pour_tous(seq_bool: list[bool]) -> bool:
    """
    Renvoie True ssi `seq_bool` ne contient pas False

    Exemples:

    $$$ pour_tous([])
    True
    $$$ pour_tous((True, True, True))
    True
    $$$ pour_tous((True, False, True))
    False
    """
    return not False in seq_bool


def il_existe(seq_bool: list[bool]) -> bool:
    """

    Renvoie True si seq_bool contient au moins une valeur True, False sinon

    Exemples:

    $$$ il_existe([])
    False
    $$$ il_existe((False, True, False))
    True
    $$$ il_existe((False, False))
    False
    """
    return  True in seq_bool


from etudiant import Etudiant
from date import Date

def charge_fichier_etudiants(fname: str) -> list[Etudiant]:
    """
    Renvoie la liste des étudiants présents dans le fichier dont
    le nom est donné en paramètre.

    précondition: le fichier est du bon format.
    """
    res = []
    with open(fname, 'r') as fin:
        fin.readline()
        ligne = fin.readline()
        while ligne != '':
            nip, nom, prenom, naissance, formation, groupe = ligne.strip().split(';')
            y, m, d = naissance.split('-')
            date_naiss = Date(int(d.lstrip('0')), int(m.lstrip('0')), int(y))
            res.append(Etudiant(int(nip), nom, prenom, date_naiss, formation, groupe))
            ligne = fin.readline()
    return res

L_ETUDIANTS =  charge_fichier_etudiants("etudiants.csv")
COURTE_LISTE = L_ETUDIANTS[0:11]

def est_liste_d_etudiants(x:list) -> bool:
    """
    Renvoie True si ``x`` est une liste de fiches d'étudiant, False dans le cas contraire.

    Précondition: aucune

    Exemples:

    $$$ est_liste_d_etudiants(COURTE_LISTE)
    True
    $$$ est_liste_d_etudiants("Timoleon")
    False
    $$$ est_liste_d_etudiants([('12345678', 'Calbuth', 'Raymond', 'Danse', '12') ])
    False
    """
    return all(isinstance(i,Etudiant) for i in x)

#Gestion de la promotion

#question 4 >>> len(L_ETUDIANTS)
#603

NBR_ETUDIANTS = len(L_ETUDIANTS)

#question 5
NIP = 42206735
NIP%NBR_ETUDIANTS
# Alexandre Maillard est a lindice formé par mon Nip modulo nombre detudiant 
#question 6

moinsde20ans = list(etudiant for etudiant in L_ETUDIANTS if etudiant.naissance > Date(2, 2, 2004))
#[L_ETUDIANTS[i] for i in range(NBR_ETUDIANTS) if L_ETUDIANTS[i].naissance>Date(2,2,2004)]

#question 7
def ensemble_des_formations(liste: list[Etudiant]) -> set[str]:
    """
    Renvoie un ensemble de chaînes de caractères donnant les formations
    présentes dans les fiches d'étudiants

    Précondition: liste ne contient que des fiches d'étudiants

    Exemples:

    $$$ ensemble_des_formations(COURTE_LISTE)
    {'MI', 'PEIP', 'MIASHS', 'LICAM'}
    $$$ ensemble_des_formations(COURTE_LISTE[0:2])
    {'MI', 'MIASHS'}
    """
    return {L_ETUDIANTS[i].formation for i in range(len(liste))}
    
  
#question 8


def nb_occurence(liste:list[Etudiant]) -> dict():
    """donne le nombre d'occurence du prenom des etudiants de la promo

    Précondition : 
    Exemple(s) :
    $$$ nb_occurence(L_ETUDIANTS)['Louis']
    2
    $$$ nb_occurence(L_ETUDIANTS)['Alexandre']
    3
    """
    res = {}
    for etud in liste:
        prenom = etud.prenom
        if prenom in res:
            res[prenom] = res[prenom] + 1
        else:
            res[prenom] = 1
    return res

# Il y'a 3 Alexandre et 2 Camille
#La liste necessite un parcours 

#question 9 il y'a 198 prenoms differents car len(nb_occurence(L_ETUDIANTS)) == 198

#question 10 le prenom le plus frequent est Margot avec 9 personnes suivit par Denise et Theodore avec 8 personnes chacun se prenommant
#for nom, occurence in res.items(): en utilisant ce petit bout de code qui permet de donné a partir dun nombre minimum doccurence 
    #if occurence >=8:
        #print(nom,occurence)

#question 11
def occurrences_nip(L_ETUDIANTS) -> bool:
    """
    renvoie True si tout les nips sont unique sinon False
    Précondition : 
    Exemple(s) : L_ETUDIANTS liste d'etudiant
    $$$ 

    """
    return len(set(etudiant.nip for etudiant in L_ETUDIANTS)) == len(L_ETUDIANTS)
#les nips sont bien tous unique

# question 12
def occurence_formation(list:list[Etudiant]) -> dict():
    """
    Renvoie le nombre de personne dans chaque formation

    Précondition : 
    Exemple(s) :
    $$$ 

    """
    
    res = {}
    for etud in liste:
        formation = etud.formation
        if formation in res:
            res[formation] = res[formation] + 1
        else:
            res[formation] = 1
    return res
        
#occurence_formation(L_ETUDIANTS)
#{'MIASHS': 219, 'MI': 288, 'PEIP': 76, 'LICAM': 20}
#la formation possede 219 en MIASHS, 288 en MI , 76 en PEIP et 20 en LICAM

#Question 13

def liste_formation(liste: list[Etudiant], form: str) -> list[Etudiant]:
    """
    Renvoie la liste des étudiants de la formation ``form``

    Précondition:  liste ne contient que des fiches d'étudiants

    Exemples:

    $$$ l_MI = liste_formation(COURTE_LISTE, 'MI')
    $$$ len(l_MI)
    7
    $$$ type(l_MI[1]) == Etudiant
    True
    $$$ len(liste_formation(L_ETUDIANTS, 'INFO'))
    0
    """
    res = []
    for i in liste:
        if i.formation == form:
            res.append(i)
    return res 