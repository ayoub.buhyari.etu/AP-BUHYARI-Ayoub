#BUHYARI Ayoub
#MI15

from ap_decorators import *

@trace
def binomial(n,p):
    """

    Précondition :  n>0
    Exemple(s) :
    $$$ binomial(5,4)
    5
    $$$ binomial(5,0)
    5
    """
    if n == p:
        return 1
    elif p==1:
        return n
    elif p==0:
        return n
    else:
        return binomial(n-1,p-1) + binomial(n-1,p)
    
@trace    
def is_palindromic(chaine):
    """Renvoie le palindrome d'une chaine
    Précondition :
    Exemple(s) :
    $$$ is_palindromic("kayak")
    True
    $$$ is_palindromic("abc")
    False
    """
    if len(chaine) ==1:
        return True
    elif chaine[0] == chaine[-1] and is_palindromic(chaine[1:-1]):
        return True
    else:
        return False

@trace
def somme(a,b):
    """Renvoie la somme de 2 chiffres

    Précondition : 
    Exemple(s) :
    $$$ somme(3,4)
    7
    $$$ somme(3,0)
    3
    $$$ somme(0,3)
    3
    $$$ somme(4,3)
    7
    """
    if a==0:
        return b
    elif b==0:
        return a 
    elif a<b:
        return somme(a-1,b+1)
    elif b<a:
        return somme(a+1,b-1)
    


    

    