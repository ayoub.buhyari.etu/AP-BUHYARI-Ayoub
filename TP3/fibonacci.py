#Buhyari Ayoub
#MI15

#Nombres de Fibonacci
from ap_decorators import *

@count
def fibo(n):
    """ calcule pour tout n la suite de Fibonacci

    Précondition : n>=0
    Exemple(s) :
    $$$ fibo(0)
    0
    $$$ fibo(1)
    1
    $$$ fibo(2)
    1
    $$$ fibo(3)
    2
    $$$ fibo(4)
    3
    $$$ fibo(5)
    5
    $$$ fibo(6)
    8
    $$$ fibo(7)
    13
    $$$ fibo(8)
    21
    $$$ fibo(9)
    34
    $$$ fibo(10)
    55
    """
    if n == 0:
        return 0
    elif n == 1:
        return 1
    else:
        return fibo(n-1) + fibo(n-2)
    
    
# pour fibo(40) le calcule est couteux

#>>> fibo.counter = 0
#>>> fibo(40)
#102334155
#>>> fibo.counter
#331160281

#>>> for i in range(11):
#    fibo.counter = 0
# print("pour", "f de", i, "ca renvoie", fibo(i), " et calcule", fibo.counter, "x")
    
#pour f de 0 ca renvoie 0  et calcule 1 x
#pour f de 1 ca renvoie 1  et calcule 1 x
#pour f de 2 ca renvoie 1  et calcule 3 x
#pour f de 3 ca renvoie 2  et calcule 5 x
#pour f de 4 ca renvoie 3  et calcule 9 x
#pour f de 5 ca renvoie 5  et calcule 15 x
#pour f de 6 ca renvoie 8  et calcule 25 x
#pour f de 7 ca renvoie 13  et calcule 41 x
#pour f de 8 ca renvoie 21  et calcule 67 x
#pour f de 9 ca renvoie 34  et calcule 109 x
#pour f de 10 ca renvoie 55  et calcule 177 x


















