import turtle
from turtle import *

def zigzag(n):
    """à_remplacer_par_ce_que_fait_la_fonction

    Précondition : 
    Exemple(s) :
    $$$ 

    """
    if n >0 :
        turtle.left(45)
        turtle.forward(50)
        turtle.right(90)
        turtle.forward(50)
        turtle.left(45)
        return zigzag(n-1)
    
#courbe de von koch

def courbedevonkoch(n:int,l):
    """trace la courbe de von koch de longueur l a l'ordre n

    Précondition : 
    Exemple(s) :
    $$$ 

    """
    if n==0:
        turtle.forward(l)
    else:
        koch(n-1,l/3)
        turtle.left(60)
        koch(n-1,l/3)
        turtle.right(120)
        koch(n-1,l/3)
        turtle.left(60)
        koch(n-1,l/3)



def flacondeVonKoch(l,n:int):
    """trace le flacon de von koch selon different ordre 

    Précondition : l float ou entier et n>0 et l>0
    Exemple(s) :
    $$$ 

    """
    turtle.speed(500)
    koch(n,l)
    turtle.right(120)
    koch(n,l)
    turtle.right(120)
    koch(n,l)
    turtle.right(120)
    
    
#cest la meme relation pour ces 2 tracé juste la longueur change
#la relation la relation entre la longueur de tracé à l'ordre n  et à l'ordre  + 1
    #est donné par ce calcul:
    #((4/3)**n*l)/((4/3)**(n+1)*l) = ((4/3)**n*l)/(4/3*(4/3)**n**1) = 3/4
    
def courbedecesaro(l,n:int):
    """dessine la courbe de cesaro a lordre n

    Précondition :  l et n >0 et l doit etre soit un entier soit un float
    Exemple(s) :
    $$$ 

    """
    if n==0:
        turtle.forward(l)
    else:
        courbedecesaro(l/3,n-1)
        turtle.left(80)
        courbedecesaro(l/3,n-1)
        turtle.right(160)
        courbedecesaro(l/3,n-1)
        turtle.left(80)
        courbedecesaro(l/3,n-1)


#a chaque fois que on divise par 2 la longueur on diminie de n-1 pour reafaire un triangle a partir de ce milieu 
def Triangledesierpinski(l,n:int):
    """dessine le triangle de Sierpinski de longueur l et d'ordre n

    Précondition : l et n >0 et l est un entier ou un float
    Exemple(s) :
    $$$ 

    """
    if n ==0:
        turtle.forward(l)
        turtle.left(120)
        turtle.forward(l)
        turtle.left(120)
        turtle.forward(l)
        turtle.left(120)
    if n>0:
        Triangledesierpinski(l/2,n-1)
        turtle.forward(l/2)
        Triangledesierpinski(l/2,n-1)
        turtle.backward(l/2)
        turtle.left(60)
        turtle.forward(l/2)
        turtle.right(60)
        Triangledesierpinski(l/2,n)
        turtle.left(60)
        turtle.backward(l/2)
        turtle.right(60)
        
